﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guess
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Random generator = new Random();
            int tries = 5;
            int guess = 0;
            int tryNumb = 0;
            
            // If the number within the next is n, the number generated goes to 0 to n-1
            //for (int i = 0; i < 50; i++)
            //{
            //    int randomNum2 = generator.Next(100);
            //    System.Console.Write(randomNum2+ "\t");
            //
            //}
           // System.Console.ReadLine();
            int randomNum = generator.Next(100)+1;

            System.Console.WriteLine(randomNum);
            System.Console.WriteLine("Guess the computers random number 1-100");
            while (true)
            {
                guess = Convert.ToInt32(Console.ReadLine());
                tryNumb++;
                if (guess == randomNum)
                {
                    Console.WriteLine("Correct");
                    break;
                }
                if (guess > randomNum)
                {
                    Console.WriteLine("Guess was too high");
                }
                if (guess < randomNum)
                {
                    Console.WriteLine("Guess was too low");
                }
                if (tryNumb == tries)
                {
                    Console.WriteLine($"The number was: {randomNum}");
                    break;
                }
                Console.Write($"There are {tries - tryNumb} tries remaining. Enter another number: ");
            }
            System.Console.ReadLine();
        }
    }
}